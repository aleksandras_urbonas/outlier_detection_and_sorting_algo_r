cat('# 2. Preview Data:\n')

load('2_data/d_raw.Rdata')
# backup
d <- d_raw
rm(d_raw)

# Preview contents of data for quick insights
str(d)

# Summarize
summary(d)

# Id: to char
# Views: OK .. 0 to 8861
# Sales: OK .. 0 to 45
# Price: needs cleaning
# Age: OK .. 0 to 1479
# CRate: OK, mainly 5, so no value .. 0 to 10
# Quality: OK, with decimals, some are 0
# Images: OK, some are 0 (!)
# Formats: OK, 1 .. 15

# cleanup format

# fix type
d$Id <- as.character(d$Id)
# set as row names
row.names(d) <- d$Id
# remove
d$Id <- NULL

# round, because some records contain .
d$Views <- round(d$Views)

# Price
d$Price <-
  as.numeric(
    sub(
      ","
      , ""
      , trimws(d$Price)
      , fixed = T
    )
  )

# round to whole number
d$Quality <- round(d$Quality)
# categorize
d$Quality <- as.factor(d$Quality)


# save to file
save(
  d
  , file='2_data/d_2.Rdata'
)
cat('data 2 saved to file.\n')


cat('Next...\n')