cat('# 3. Univariate Review:\n\n')


load('2_data/d_2.Rdata')
cat('data 2 loaded.\n\n')


cat('## Update features one by one:\n\n')


(x <- 'Views')

head(d[,x])
cat('first rows contain at least 6000 views each\n\n')


# boxplot
boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
cat('even distribution, no action\n\n')

cat('... but many views is not typical, check outliers:\n\n')

out <- unique(boxplot.stats(d[,x])$out)

min(out)
# 1090 views is already rare
length(out)


par(mfrow=c(1,2))

# distribution
hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab='x=Views'
  , ylab='y=Ratio'
)

# density
plot(
  density(d[,x])
  , main='Density plot'
  , sub='(c) A.U., 2018'
  , xlab=x
)
par(mfrow=c(1,1))

cat('exponentially decreasing...\n\n')



cat('## new feature: consider log10 transformation\n')
cat('(easier to interpret data in log10 scale, which fixes skewed distributions\n\n')

(x <- 'Views_log10')
(xx <- 'Views')

d[,x] <- log10(d[,xx])

boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
# few outliers


cat('# Exclude outliers:\n\n')
out <- unique(boxplot.stats(d[,x])$out)
# id outliers by views
idx <- which(d[,x]%in%out)
# if any id'ed
if(length(idx)>0) {
  d <- d[-idx,]
  cat(
    length(idx)
    , 'outliers removed by'
    , x
    , '\n\n'
  )
}

par(mfrow=c(1,2))

# distribution
hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab='x=Views'
  , ylab='y=Ratio'
)

# density
plot(
  density(d[,x])
  , main='Density plot'
  , sub='(c) A.U., 2018'
  , xlab=x
)
par(mfrow=c(1,1))

cat('Histogram reveals: more items of higher quality.\n\n')
cat('items can be grouped to four groups: # <1, 1-2, 2-3, and 3+\n\n')



(x <- 'Sales')

head(d[,x])
cat('contains `0`` .. mark as not sold\n\n')

summary(d[,x])
cat('0 to 45.\n\n')

par(mfrow=c(1,2))
boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)

plot(
  density(d[,x])
  , main='Density plot'
  , sub='(c) A.U., 2018'
  , xlab=x
)
par(mfrow=c(1,1))
cat('most are not sold, and sales are outliers.\n\n')

cat('checking outliers:\n\n')
out <- sort(unique(boxplot.stats(d[,x])$out))

cat('Considering data distribution, log10 is proposed.\n\n')
cat('Drawing plots:\n\n')
par(mfrow=c(1,2))

hist(
   log10(d[,x])
  , main='Distribution plot'
  , sub='(c) A.U. 2018'
  , xlab=paste('log10 of',x)
  , ylab='Date'
)

plot(
  density(d[,x])
  , main='Density plot'
  , sub='(c) A.U., 2018'
  , xlab=paste('log10 of',x)
)
par(mfrow=c(1,1))


cat('## New Feature: is Sold?\n\n')
(xx <- 'is_sold')

idx <- which(d[,x]==0)

if(length(idx)>0) {
  # everyone is sold
  1 -> d[,xx]
  # except several
  0 -> d[idx,xx]
  cat(
    length(idx)
    , 'rows updated'
  )
  d[,xx] <- factor(d[,xx])
}
cat('This feature can help us understand what makes people buy more.\n')
cat('Later we can compare each feature by this new flag `is_sold`.\n')

table(d[,xx])



(x <- 'Price')

head(d[,x])

summary(d[,x])

quantile(d[,x], probs=c(0,0.01,0.25,0.50,0.99,1))

par(mfrow=c(1,2))
boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
cat('contains outliers.\n\n')


hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab='x=Views'
  , ylab='y=Ratio'
)
cat('Exponentially decreasing.\n\n')
par(mfrow=c(1,1))


cat('check outliers:\n\n')
out <-
  sort(
    unique(
      boxplot.stats(d[,x])$out
    )
  )

# lowest values
head(out)

# largest values
tail(out)



(x <- 'Age')

head(d[,x])

summary(d[,x])

quantile(d[,x])

par(mfrow=c(1,2))
boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
cat('contains outliers, which are older records.\n\n')

hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab='x=Views'
  , ylab='y=Ratio'
)
cat('Decreasing.\n\n')
par(mfrow=c(1,1))


cat('Checking outliers:\n\n')
out <- sort(unique(boxplot.stats(d[,x])$out))
cat('An outlier by',x,'is considered to be at least', min(out))



(x <- 'CRate')

head(d[,x])
# all 5's

summary(d[,x]) # 0.. 10

table(d[,x]) # most have no info

par(mfrow=c(1,2))
boxplot(
  d[,x]
  , main='Distribution'
  , sub='(c) A.U. 2018'
  , xlab=x
  , ylab='Date'
)
cat('Records with info are outliers.\n\n')

hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab='x=Views'
  , ylab='y=Ratio'
)
cat('Records with info are outliers.\n\n')
par(mfrow=c(1,1))


cat('## New Feature: Is rated?\n\n')

(xx <- 'is_CRate')
# id all non-rated
idx <- which(d[,x]!=5)
if(length(idx)>0) {
  # nobody is sold
  0 -> d[,xx]
  # except the id'ed
  1 -> d[idx,xx]
  cat(
    length(idx)
    , 'rows updated'
    , x
    , '\n\n'
  )
  # categorize
  d[,xx] <- factor(d[,xx])
  # # exclude id'ed
  # d <- d[idx,]
  # # inform
  # cat(
  #   length(idx)
  #   , 'rows removed.\n'
  # )
  # 0 -> d[,xx]
  # 1 -> d[idx,xx]
}

summary(d[,xx]) # only 1 percent
cat(x, 'could also important, because unrated data is not validated.\n')
cat('Such data cannot be relied upon until more input is collected.\n\n')



(x <- 'Quality')

# review
head(d[,x])

cat('Round the Quality score:\n\n')
d[,x] <- as.integer(d[,x]) - 1


boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
cat('One outlier.\n\n')



cat('Review distibution:\n\n')
pt <-
  round(
    prop.table(
      table(
        d[,x]
      )
    ) * 100
    , 1
  )
# review
pt
x

cat('Plot the distribution:\n\n')
png(
  paste0('3_figures/3_', x, '.png')
)

barplot(
  pt
  , main='Quality distribution'
  , sub = 'Aleksandras Urbonas, 2018'
  , xlab = 'Quality score'
  , ylab = 'ratio of total population'
)
dev.off()
cat('figure saved to file.\n\n')

cat('Rather unusual distribution:\n')
cat('- a lot of average (5) values;\n')
cat('- a lot of higher scores (7, 8, 9).\n\n')


## Experiment
# match 'Quality' and 'CRate', if both =5, call 'unreviewed'
# idx <- which(d$Quality==5)
# length(idx)
# idx <- which(d$CRate==5)
# length(idx)


(x <- 'Images')

head(d[,x])
cat('.. contains 0s.\n\n')


summary(d[,x])
cat('0 to 958.\n\n')

par(mfrow=c(1,2))
hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab=x
  , ylab='y=Ratio'
)
cat('Most have no info.\n\n')
# table(d[,x]) # most have no info


boxplot(
  d[,x]
  , main=x
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
# cat('One outlier.\n\n')
par(mfrow=c(1,1))


boxplot(
  log10(d[,x])
  , main=paste('log10 of', x)
  , sub='(c) A.U. 2018'
  , ylab='Date'
)
cat('better representation, few outliers.\n\n')


cat('Check outliers:\n\n')
out <- unique(boxplot.stats(log10(d[,x]))$out)
idx <- which(log10(d[,x])%in%out)
if(length(idx)>0) {
  d <- d[-idx,]
  cat(
    length(idx)
    ,'outliers removed by'
    , paste('log10 of', x)
    , '\n\n'
  )
}



(x <- 'Formats')

head(d[,x])

summary(d[,x]) # 1..15

hist(
  d[,x]
  , main='Distribution plot'
  , sub='(c) A.U., 2018'
  , xlab=x
  , ylab='y=Ratio'
)
cat('uniform distribution, except first group is twice as large\n\n')


table(d[,x])
cat('even distribution, no further actions.\n\n')



## Finalize chapter.
stopifnot(
  length(
    which(
      !complete.cases(d)
    )
  ) == 0
)
cat('Data contains no missing values.\n\n')


save(
  d
  , file='2_data/d_3.Rdata'
)
cat('data 2 saved to file.\n\n')


# cleanup
rm(idx, out, pt, x, xx)


cat('Next...\n\n')
