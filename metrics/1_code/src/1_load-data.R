cat('# 1. Load data:\n')


d_raw <-
  read.csv(
    "4_raw-data/model-metrics.csv"
    , fileEncoding = "UTF-8"
    , quote = "\""
  )
cat('raw data loaded.\n')


save(
  d_raw
, file='2_data/d_raw.Rdata'
)
cat('raw data saved to file.\n')


cat('Next...\n')