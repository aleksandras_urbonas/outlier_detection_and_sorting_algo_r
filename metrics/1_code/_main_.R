# cgtrader - data scientist task 2 - model metrics
# (c) Aleksandras Urbonas, 2018 ####


# Clean start
rm(list=ls())

# Set project dir
setwd('d:/cgtrader_ds/metrics/')


## 0. Load libraries
source('1_code/src/0_libraries.R')


## 1. Load data
# source('1_code/src/1_load-data.R')


## 2. Preview data
# source('1_code/src/2_preview-data.R')


## 3. Univariate Review
# source('1_code/src/3_univariate-review.R')


## 4. Advanced analysis
# source('1_code/src/4_advanced-review.R')
# Analyze reviewed features


## 5. Models
# 1. Check if CRate is important (yes)
# 2. Check importance of CRATE (lowest)
# 3. Check model for all records

source('1_code/src/5_design-models.R')


cat('End of file.\n\n')