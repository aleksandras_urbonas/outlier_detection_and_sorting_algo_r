cat('# 4. Transform Data:\n\n')


load('2_data/d_3.Rdata')
cat('data 3 loaded.\n\n')


# sample some records
set.seed(1235)

sample_size <- 1000

idx <-
  sample(
    1:nrow(d)
    , sample_size
  )

if(length(idx)>0) {
  d <- d[idx,]
  cat(
    sample_size
    ,'rows sampled.\n\n'
  )
}

# structure
str(d)

# analyze keywords
keep_x <- c('results_count','keywords','is_found')
d <- d[,keep_x]

## So, which keywords are popular?
# For now, let us focus on keywords and results.

# group by (search) count to A=best, B=median, C=low
# group by sum to A=best, B=median, C=low

# if count=A, then often
# if count=B, then average
# if count=C, then rarely

# if sum=A, then many
# if sum=B, then enough
# if sum=C, then little

## only keep popular keywords
# load top keywords
# load('2_data/top_keywords.Rdata')
# (x <- 'keywords')
# idx <- which(d[,x]%in%top_keywords)
# if(length(idx)>0)
#   d <- d[idx,]
#   cat(length(idx),'rows remaining\n')

my_groups <- c(3, 2, 1)
my_probs <- c(0, 0.8, 0.9, 1)

source('1_code/src/4A_sum.R')
source('1_code/src/4B_count.R')

## combine a_sum and a_count
agg <-
  merge(
    a_count
    , a_sum
    , by = 'keywords'
    , all.x = T
  )

# preview
head(agg)


# save to file
save(
  agg
  , file='2_data/agg_4.Rdata'
)
cat('agg 4 data saved to file.\n\n')


# cleanup
rm(keep_x, by, how, what, into, x, xx, idx)


cat('Next...\n\n')
