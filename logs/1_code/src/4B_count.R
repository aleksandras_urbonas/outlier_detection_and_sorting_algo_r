cat('# 4B. Count\n')

# find sum of results by keyword ####
(what <- 'results_count')
(by <- 'keywords')
(how <- 'count')
(
  into <- paste(
    what
    , how
    , sep='__'
  )
)

# aggregate
a_count <-
  aggregate(
    d[,what]
    , by=list(d[,by])
    , FUN=length
  )
names(a_count) <- c(by, into)
head(a_count)

## Cut into groups
(x <- 'found_group')
(xx <- into)


# group to A, B, C
quantiles <-
  quantile(
    a_count[,xx]
    , probs=my_probs
  )
# force lower limit to be 0
quantiles[1] <- 0

a_count[,x] <- 
  cut(
    x=a_count[,into]
    , breaks=quantiles
    , labels=my_groups
  )

# review
head(a_count)
# some are null

# id NA
idx <- which(is.na(a_count[,x]))
# if any id'ed
if(length(idx)>0) {
  # set as lowest, first in list
  my_groups[1] -> a_count[idx,x]
  # inform
  cat(
    length(idx)
    , 'rows updated in'
    , x
	, '\n\n'
  )
}
a_count[,x] <- factor(a_count[,x])
summary(a_count)
