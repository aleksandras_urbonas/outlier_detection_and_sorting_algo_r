cat('# 2. Data Preview:\n\n')


# load data
load('2_data/d_raw.Rdata')
cat('raw data loaded.\n\n')

# Create a working file as copy of raw data
d <- d_raw
# remove used copy
rm(d_raw)


# Preview contents of data for quick insights
str(d)


# Features
cat(
  'features:'
  , names(d)
  , '\n\n'
)

# actions by feature:
# created_at: convert to date
# id: convert to char
# user_id: convert to char; nulls are given
# ip_address: convert to char
# keywords: convert to char
# results_count: convert to number

d$created_at <-
  as.POSIXct(
    d$created_at
    , format='%Y-%m-%d %H:%M'
    )

d$id <- as.character(d$id)
row.names(d) <- d$id
d$id <- NULL

d$user_id <- as.character(d$user_id)

d$ip_address <- as.character(d$ip_address)

d$keywords <- as.character(d$keywords)

(x <- 'results_count')
d[,x] <- as.character(d[,x])

# clean empty
idx <- which(d[,x] == '')
if(length(idx)>0) {
  '0' -> d[idx,x]
  cat(
    length(idx)
  , 'empty rows updated by'
  , x
  , '\n\n'
  )
}

# clean "(null)"
idx <- which(d[,x] == '(null)')
if(length(idx)>0) {
  '0' -> d[idx,x]
  cat(
    length(idx)
    , 'empty rows updated'
    , x
    , '\n\n'
  )
}

d[,x] <- as.integer(d[,x])
idx <- which(is.na(d[,x]))
if(length(idx)>0) head(d[idx,])
# summary(d$results_count)


cat('Feature types corrected.\n\n')

# structure after update
str(d)

# summaries
summary(d)
# initial review looks ok, moving on to next step


# save data
save(
  d
, file='2_data/d_2.Rdata'
)
cat('data 2 saved to file.\n\n')


cat('Next...\n\n')
